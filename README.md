# Flammie’s gentoo overlay

An overlay for gentoo for things Flammie's worked on: Apertium, HFST,
computational linguistics, spell-checking, obscure languages...
