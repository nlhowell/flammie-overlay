# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{4,5,6,7} )

inherit distutils-r1

DESCRIPTION="a tool for unsupervised and semi-supervised morphological segmentation"
HOMEPAGE="http://morpho.aalto.fi"
SRC_URI=""

if [ "${PV}" = "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/aalto-speech/morfessor.git"
	#EGIT_COMMIT="refs/tags/${PV}"
else
	#SRC_URI="https://morpho.aalto.fi/projects/morpho/${P}.tar.gz"
	SRC_URI="https://github.com/aalto-speech/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~arm ~x86 ~amd64"
IUSE="doc"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="doc? ( dev-python/sphinx[${PYTHON_USEDEP}]
	dev-python/sphinxcontrib-napoleon[${PYTHON_USEDEP}]
)"
